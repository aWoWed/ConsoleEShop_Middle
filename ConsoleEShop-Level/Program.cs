﻿using System;
using ConsoleEShop_Level.Controllers;
using ConsoleEShop_Level.Models;
using ConsoleEShop_Level.Repositories;
using ConsoleEShop_Level.Services;
using ConsoleEShop_Level.Views;
using AppContext = ConsoleEShop_Level.Models.AppContext;

namespace ConsoleEShop_Level
{
    class Program
    {
        static void Main(string[] args)
        {
            var console = new ConsoleView();

            var appContext = new AppContext();
            var serviceManager = new ServiceManager();
            var dataManager = new DataManager(appContext, serviceManager);

            var controller = new MainController(console, dataManager);

            controller.Start();
        }
    }
}
