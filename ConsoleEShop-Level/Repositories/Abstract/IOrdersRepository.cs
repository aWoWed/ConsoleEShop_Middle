﻿using System.Collections.Generic;
using ConsoleEShop_Level.Models;

namespace ConsoleEShop_Level.Repositories
{
    public interface IOrdersRepository : IRepository<Order>
    {
        Order GetOrderByUserId(int userId);
        List<Order> GetOrdersByUserId(int userId);
    }
}
