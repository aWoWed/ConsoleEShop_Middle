﻿using System.Collections.Generic;
using ConsoleEShop_Level.Models;

namespace ConsoleEShop_Level.Repositories
{
    public interface IProductsRepository : IRepository<Product>
    {
        Product GetProductByName(string name);
        Product GetProductByNameCategoryDescriptionPrice(string name, string category, string description, decimal price);
    }
}
