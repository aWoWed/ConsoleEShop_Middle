﻿
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleEShop_Level.Models;
using ConsoleEShop_Level.Services;


namespace ConsoleEShop_Level.Repositories
{
    public class DataManager
    {
        public IUsersRepository Users { get; }
        public IProductsRepository Products { get; }
        public IOrdersRepository Orders { get; }

        public DataManager(AppContext context, ServiceManager serviceManager)
        {
            Users = new UsersRepository(context, serviceManager);
            Products= new ProductsRepository(context, serviceManager);
            Orders = new OrdersRepository(context, serviceManager);
        }
    }
}
