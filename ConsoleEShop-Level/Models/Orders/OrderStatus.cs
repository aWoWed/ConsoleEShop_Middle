﻿namespace ConsoleEShop_Level.Models
{
    public enum OrderStatus
    {
        New,
        CanceledByAdmin,
        CanceledByUser,
        ReceivedPayment,
        Sent,
        Received,
        Done
    }
}
